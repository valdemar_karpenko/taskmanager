﻿using System;
using Microsoft.AspNetCore.Mvc;
using TaskManager.ViewModels;
using TaskManager.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TaskManager.HelperMethods;

namespace TaskManager.Controllers
{
    public class HomeController : Controller
    {
        private TaskManagerContext db;
        public HomeController(TaskManagerContext context)
        {
            db = context;
        }
        public async Task<IActionResult> Index()
        {
            return View(new TasksTreeViewModel { Tasks = await db.Tasks.ToListAsync() });
        }

        public async Task<IActionResult> Info(int? taskId)
        {
            if (taskId != null)
            {
                Models.Task task = await db.Tasks.FirstOrDefaultAsync(t => t.Id == taskId);
                if (task != null)
                {
                    TaskHelpers taskHelpers = new TaskHelpers();                  
                    var totalChildTasksActualTime = new TimeSpan(await taskHelpers.CalcTotalChildTasksActualTime(db, task.Id));
                    var totalChildTasksPlannedTime = new TimeSpan(await taskHelpers.CalcTotalChildTasksPlannedTime(db, task.Id));
                    await taskHelpers.CalcActualPerformanceTime(db, task);
                    await db.SaveChangesAsync();
                    ViewBag.CanCompleteTask = taskHelpers.CanCompleteTask(db, task);
                    return View(new TasksViewModel { AllTasks = await db.Tasks.ToListAsync(), TargetTask = task, TotalChildTasksActualTime = totalChildTasksActualTime, TotalChildTasksPlannedTime = totalChildTasksPlannedTime });
                }             
            }
            return NotFound();
        }

        public async Task<IActionResult> Create(int? taskId)
        {
            var taskEditViewModel = new TaskEditViewModel();
            if (taskId != null)
            {              
                var parentTask = await db.Tasks.FirstOrDefaultAsync(t => t.Id == taskId);
                ViewBag.ParentTask = parentTask.Name;              
                if (parentTask != null)
                {                 
                    taskEditViewModel.ParentId = parentTask.Id;
                    return View(taskEditViewModel);
                }                  
            }
            return View(taskEditViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(TaskEditViewModel taskEditViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var taskToCreate = new Models.Task();
                    taskToCreate.RegistrationDate = DateTime.Now;
                    taskToCreate.PlannedPerformanceTime = new TimeSpan(taskEditViewModel.PlannedDays, taskEditViewModel.PlannedHours, taskEditViewModel.PlannedMinutes, 0);
                    taskToCreate.ParentId = taskEditViewModel.ParentId;
                    taskToCreate.Name = taskEditViewModel.Name;
                    taskToCreate.Description = taskEditViewModel.Description;
                    taskToCreate.Performers = taskEditViewModel.Performers;
                    db.Tasks.Add(taskToCreate);
                    await db.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException)
            {
                ModelState.AddModelError("", "Ошибка при сохранении изменений. Ппробуйте выполнить запрос ещё раз.");
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "На сервере произошла ошибка. Ппробуйте выполнить запрос ещё раз.");
            }
            var parentTask = await db.Tasks.FirstOrDefaultAsync(t => t.Id == taskEditViewModel.ParentId);
            if(parentTask!=null)
                ViewBag.ParentTask = parentTask.Name;
            return View(taskEditViewModel);
        }


        public async Task<IActionResult> Edit(int? taskId)
        {
            if (taskId != null)
            {
                Models.Task task = await db.Tasks.FirstOrDefaultAsync(t => t.Id == taskId);
                if (task != null)
                    return View(new TaskEditViewModel
                    {
                        Id = task.Id,
                        ParentId = task.ParentId,
                        Name = task.Name,
                        Description = task.Description,
                        Performers = task.Performers,
                        PlannedDays = task.PlannedPerformanceTime.Days,
                        PlannedHours = task.PlannedPerformanceTime.Hours,
                        PlannedMinutes = task.PlannedPerformanceTime.Minutes
                    });
            }
            return NotFound();
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(TaskEditViewModel taskEditViewModel, int? taskId)
        {
            var taskToUpdate = await db.Tasks.SingleOrDefaultAsync(
                t => t.Id == taskId);
            try
            {
                if (ModelState.IsValid)
                {
                    taskToUpdate.Name = taskEditViewModel.Name;
                    taskToUpdate.Description = taskEditViewModel.Description;
                    taskToUpdate.Performers = taskEditViewModel.Performers;
                    taskToUpdate.PlannedPerformanceTime = new TimeSpan(taskEditViewModel.PlannedDays, taskEditViewModel.PlannedHours, taskEditViewModel.PlannedMinutes, 0);
                    db.Tasks.Update(taskToUpdate);
                    await db.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException)
            {
                ModelState.AddModelError("", "Ошибка при сохранении изменений. Ппробуйте выполнить запрос ещё раз.");
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "На сервере произошла ошибка. Ппробуйте выполнить запрос ещё раз.");
            }

            return View(taskEditViewModel);
        }

        [HttpGet]
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDelete(int? taskId)
        {
            if (taskId != null)
            {
                if(await db.Tasks.AnyAsync(t => t.ParentId == taskId))
                    return NotFound();
                Models.Task task = await db.Tasks.FirstOrDefaultAsync(t => t.Id == taskId);
                if (task != null)
                    return View(task);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? taskId)
        {
            if (taskId != null)
            {
                Models.Task task = await db.Tasks.FirstOrDefaultAsync(t => t.Id == taskId);
                if (task != null)
                {
                    db.Tasks.Remove(task);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            return NotFound();
        }

        
        public async Task<IActionResult> StartTask(int? taskId)
        {
            if (taskId != null)
            {
                Models.Task task = await db.Tasks.FirstOrDefaultAsync(t => t.Id == taskId);
                if (task != null)
                {
                    TaskHelpers taskHelpers = new TaskHelpers();
                    await taskHelpers.StartTask(db, task);
                    return RedirectToAction("Info", "Home", new { taskId = taskId });
                }
            }
            return NotFound();
        }

        public async Task<IActionResult> PauseTask(int? taskId)
        {
            if (taskId != null)
            {
                Models.Task task = await db.Tasks.FirstOrDefaultAsync(t => t.Id == taskId);
                if (task != null)
                {
                    TaskHelpers taskHelpers = new TaskHelpers();
                    await taskHelpers.PauseTask(db, task);
                    return RedirectToAction("Info", "Home", new { taskId = taskId });
                }
            }
            return NotFound();
        }

        public async Task<IActionResult> CompleteTask(int? taskId)
        {
            if (taskId != null)
            {
                Models.Task task = await db.Tasks.FirstOrDefaultAsync(t => t.Id == taskId);
                if (task != null)
                {
                    TaskHelpers taskHelpers = new TaskHelpers();
                    await taskHelpers.CalcActualPerformanceTime(db, task);
                    await taskHelpers.CalcTotalChildTasksActualTime(db, task.Id);
                    await taskHelpers.TryCompleteTask(db, task);                  
                    return RedirectToAction("Info", "Home", new { taskId = taskId });
                }
            }
            return NotFound();
        }

        public IActionResult Error()
        {          
            return View();
        }
    }
}