﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using TaskManager.Models;

namespace TaskManager.Migrations
{
    [DbContext(typeof(TaskManagerContext))]
    [Migration("20171006195956_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TaskManager.Models.Task", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<TimeSpan>("ActualPerformanceTime");

                    b.Property<DateTime>("CompletedDate");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<string>("Performers");

                    b.Property<TimeSpan>("PlannedPerformanceTime");

                    b.Property<DateTime>("RegistrationDate");

                    b.Property<int?>("TaskId");

                    b.Property<int>("TaskStatus");

                    b.HasKey("Id");

                    b.HasIndex("TaskId");

                    b.ToTable("Tasks");
                });

            modelBuilder.Entity("TaskManager.Models.Task", b =>
                {
                    b.HasOne("TaskManager.Models.Task")
                        .WithMany("ChildTask")
                        .HasForeignKey("TaskId");
                });
#pragma warning restore 612, 618
        }
    }
}
