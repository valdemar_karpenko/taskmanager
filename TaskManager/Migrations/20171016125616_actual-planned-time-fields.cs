﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TaskManager.Migrations
{
    public partial class actualplannedtimefields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActualPerformanceTime",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "PlannedPerformanceTime",
                table: "Tasks");

            migrationBuilder.AddColumn<long>(
                name: "ActualPerformanceTimeTicks",
                table: "Tasks",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "PlannedPerformanceTimeTicks",
                table: "Tasks",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActualPerformanceTimeTicks",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "PlannedPerformanceTimeTicks",
                table: "Tasks");

            migrationBuilder.AddColumn<TimeSpan>(
                name: "ActualPerformanceTime",
                table: "Tasks",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "PlannedPerformanceTime",
                table: "Tasks",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));
        }
    }
}
