﻿using Microsoft.EntityFrameworkCore;
namespace TaskManager.Models
{
    public class TaskManagerContext : DbContext
    {
        public DbSet<Models.Task> Tasks { get; set; }
        public TaskManagerContext(DbContextOptions<TaskManagerContext> options)
            : base(options)
        {
        }
    }
}
