﻿using System.ComponentModel.DataAnnotations;

namespace TaskManager.Models
{
    /// <summary>
    /// Describes status of a task
    /// </summary>
    public enum TaskStatus
    {
        [Display(Name = "Appointed")]
        Appointed,
        [Display(Name = "CarriedOut")]
        CarriedOut,
        [Display(Name = "Suspended")]
        Suspended,
        [Display(Name = "Completed")]
        Completed
    }
}
