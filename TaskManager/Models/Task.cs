﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskManager.Models
{
    /// <summary>
    /// Describes Task entity
    /// </summary>
    public class Task
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Performers { get; set; }       
        public DateTime RegistrationDate { get; set; }
        public TaskStatus TaskStatus { get; set; }
        public long ActualPerformanceTimeTicks { get; set; }
        public long PlannedPerformanceTimeTicks { get; set; }
        public DateTime? CompletedDate { get; set; }
        /// <summary>
        /// Field required for calculation ActualPerformanceTime
        /// </summary>
        public DateTime? StartPerformanceDate { get; set; }

        [NotMapped]
        public TimeSpan ActualPerformanceTime
        {
            get { return TimeSpan.FromTicks(ActualPerformanceTimeTicks); }
            set { ActualPerformanceTimeTicks = value.Ticks; }
        }

        [NotMapped]
        public TimeSpan PlannedPerformanceTime
        {
            get { return TimeSpan.FromTicks(PlannedPerformanceTimeTicks); }
            set { PlannedPerformanceTimeTicks = value.Ticks; }
        }

    }
}
