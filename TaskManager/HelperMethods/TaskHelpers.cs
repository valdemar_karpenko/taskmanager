﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Models;

namespace TaskManager.HelperMethods
{
    /// <summary>
    /// Contains help methods for Task entity
    /// </summary>
    public class TaskHelpers
    {
        /// <summary>
        /// Calculates total PlannedPerformanceTime of child tasks
        /// </summary>
        /// <param name="dbcontext"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public async Task<long> CalcTotalChildTasksPlannedTime(TaskManagerContext dbcontext, int parentId)
        {
            var children = dbcontext.Tasks.Where(t => t.ParentId == parentId);
            var totalChildPlannedTime = await children.SumAsync(t => t.PlannedPerformanceTime.Ticks);
            foreach (var child in children)
            {
                totalChildPlannedTime += await CalcTotalChildTasksPlannedTime(dbcontext, child.Id);
            }
            return totalChildPlannedTime;
        }

        /// <summary>
        /// Calculates total ActualPerformanceTime of child tasks
        /// </summary>
        /// <param name="dbcontext"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public async Task<long> CalcTotalChildTasksActualTime(TaskManagerContext dbcontext, int parentId)
        {
            var children = dbcontext.Tasks.Where(t => t.ParentId == parentId);
            var totalChildActualTime = await children.SumAsync(t => t.ActualPerformanceTime.Ticks);
            Models.Task task = await dbcontext.Tasks.FirstOrDefaultAsync(t => t.Id == parentId);
            await CalcActualPerformanceTime(dbcontext, task);
            foreach (var child in children)
            {
                
                totalChildActualTime += await CalcTotalChildTasksActualTime(dbcontext, child.Id);
            }
            
            return totalChildActualTime;
        }

        /// <summary>
        /// Checks if the task can be completed
        /// </summary>
        /// <param name="dbcontext"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        public bool CanCompleteTask(TaskManagerContext dbcontext, Models.Task task)
        {          
            if (task.TaskStatus == Models.TaskStatus.Appointed/* || task.TaskStatus == Models.TaskStatus.Completed*/)
                return false;
            var children = dbcontext.Tasks.Where(t => t.ParentId == task.Id);
            foreach (var child in children)
            {
                if (!CanCompleteTask(dbcontext, child))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Completes task and its subtasks
        /// </summary>
        /// <param name="dbcontext"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        private async System.Threading.Tasks.Task CompleteTask(TaskManagerContext dbcontext, Models.Task task)
        {
            
            var targetTask = task;
            if (targetTask.TaskStatus != Models.TaskStatus.Completed)
            {
                targetTask.TaskStatus = Models.TaskStatus.Completed;
                targetTask.CompletedDate = DateTime.Now;
                dbcontext.Update(targetTask);
            }           
            var children = dbcontext.Tasks.Where(t => t.ParentId == targetTask.Id);
            foreach (var child in children)
            {
                await CompleteTask(dbcontext, child);
            }
            await dbcontext.SaveChangesAsync();
        }

        /// <summary>
        /// Tries to сomplete task and its subtasks and return true if the task can be completed, else return false
        /// </summary>
        /// <param name="dbcontext"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        public async Task<bool> TryCompleteTask(TaskManagerContext dbcontext, Models.Task task)
        {
            if (!CanCompleteTask(dbcontext, task)  || task.StartPerformanceDate == null)
                return false;
            else {
                await CompleteTask(dbcontext, task);
                return true;
            }
        }

        /// <summary>
        /// Calculates ActualPerformanceTime of the task
        /// </summary>
        /// <param name="dbcontext"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task CalcActualPerformanceTime(TaskManagerContext dbcontext, Models.Task task)
        {           
            var targetTask = task;
            if (targetTask.TaskStatus == Models.TaskStatus.Completed)
                return;
            TimeSpan tempPerformanceTime;
            if (task.StartPerformanceDate==null || targetTask.TaskStatus == Models.TaskStatus.Appointed || targetTask.TaskStatus == Models.TaskStatus.Suspended)
                tempPerformanceTime = new TimeSpan();
            else
                tempPerformanceTime = DateTime.Now - (DateTime)task.StartPerformanceDate;
            targetTask.ActualPerformanceTime += tempPerformanceTime;
            targetTask.StartPerformanceDate = DateTime.Now;
            dbcontext.Update(targetTask);
            await dbcontext.SaveChangesAsync();
        }

        /// <summary>
        /// Suspends the task and calculates its ActualPerformanceTime
        /// </summary>
        /// <param name="dbcontext"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task PauseTask(TaskManagerContext dbcontext, Models.Task task)
        {
            var targetTask = task;
            if (targetTask.TaskStatus != Models.TaskStatus.CarriedOut)
                return;
            await CalcActualPerformanceTime(dbcontext, targetTask);
            targetTask.TaskStatus = Models.TaskStatus.Suspended;
            dbcontext.Update(targetTask);
            await dbcontext.SaveChangesAsync();
        }

        /// <summary>
        /// Begins to carry out the task
        /// </summary>
        /// <param name="dbcontext"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task StartTask(TaskManagerContext dbcontext, Models.Task task)
        {           
            var targetTask = task;
            if (targetTask.TaskStatus == Models.TaskStatus.Completed)
                return;
            targetTask.TaskStatus = Models.TaskStatus.CarriedOut;
            targetTask.StartPerformanceDate = DateTime.Now;
            dbcontext.Update(targetTask);
            await dbcontext.SaveChangesAsync();
        }
    }
}
