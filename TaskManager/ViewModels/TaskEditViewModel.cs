﻿using System.ComponentModel.DataAnnotations;

namespace TaskManager.ViewModels
{
    /// <summary>
    /// Contains properties of Task entity for create/update task. 
    /// </summary>
    public class TaskEditViewModel
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }

        [Required(ErrorMessage = "NameRequired")]
        public string Name { get; set; }

        [Required(ErrorMessage = "DescriptionRequired")]
        public string Description { get; set; }

        [Required(ErrorMessage = "PerformersRequired")]
        public string Performers { get; set; }

        /// <summary>
        /// Represents days for PlannedPerformanceTime field of Task entity
        /// </summary>
        [Required(ErrorMessage = "PlannedDaysRequired")]
        [Range(0, 10000, ErrorMessage = "PlannedDaysRange")]
        public int PlannedDays { get; set; }

        /// <summary>
        /// Represents hours for PlannedPerformanceTime field of Task entity
        /// </summary>
        [Required(ErrorMessage = "PlannedHoursRequired")]
        [Range(0, 23, ErrorMessage = "PlannedHoursRange")]
        public int PlannedHours { get; set; }

        /// <summary>
        /// Represents minutes for PlannedPerformanceTime field of Task entity
        /// </summary>
        [Required(ErrorMessage = "PlannedMinutesRequired")]
        [Range(0, 59, ErrorMessage = "PlannedMinutesRange")]
        public int PlannedMinutes { get; set; }
    }
}
