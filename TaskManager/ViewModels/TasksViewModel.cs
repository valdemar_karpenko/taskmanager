﻿using System;
using System.Collections.Generic;

namespace TaskManager.ViewModels
{
    /// <summary>
    /// Contains all Tasks, target Task and total planned/actual task performance time
    /// </summary>
    public class TasksViewModel
    {
        public List<Models.Task> AllTasks { get; set; }
        public Models.Task TargetTask { get; set; }
        public TimeSpan? TotalChildTasksActualTime { get; set; }
        public TimeSpan? TotalChildTasksPlannedTime { get; set; }
    }
}
