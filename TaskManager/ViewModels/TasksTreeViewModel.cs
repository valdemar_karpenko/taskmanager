﻿using System.Collections.Generic;

namespace TaskManager.ViewModels
{
    /// <summary>
    /// Contains all Tasks and id of seed (root) task
    /// </summary>
    public class TasksTreeViewModel
    {
        /// <summary>
        /// Represents id of root task
        /// </summary>
        public int? Seed { get; set; } 
        public IEnumerable<Models.Task> Tasks { get; set; }
    }
}
